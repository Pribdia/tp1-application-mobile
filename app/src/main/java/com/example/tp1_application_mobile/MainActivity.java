package com.example.tp1_application_mobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    static final String[] animalListString = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Parametrage RecyclerView
        final RecyclerView animalRecyclerView = (RecyclerView) findViewById(R.id.animalRecyclerView);
        animalRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        animalRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        animalRecyclerView.setAdapter(new IconicAdapter());
    }

    // Adapter (controler de ligne)
    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        AnimalList animalList = new AnimalList();

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.animal_item_row, parent, false)));
        }
        @Override
        public void onBindViewHolder(RowHolder holder, int position) {

            String animalName = animalListString[position];
            Animal animal = animalList.getAnimal(animalName);

            int resId = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());

            holder.bindModel(animalName, resId);
        }
        @Override
        public int getItemCount() {
            return(animalListString.length);
        }
    }

    // Gestionnaire d'affichage d'une ligne
    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView animalItemeTextView=null;
        ImageView animalItemeImageView=null;

        RowHolder(View row) {
            super(row);
            animalItemeTextView =(TextView)row.findViewById(R.id.animalItemeTextView);
            animalItemeImageView =(ImageView)row.findViewById(R.id.animalItemeImageView);
            row.setOnClickListener(this);
        }
        @Override
        public void onClick(View v){
            final String animalName = animalItemeTextView.getText().toString();
            Intent animalView = new Intent(MainActivity.this, AnimalActivity.class);
            animalView.putExtra("animalName",animalName);
            startActivity(animalView);
        }
        void bindModel(String animalName,int resId) {

            animalItemeTextView.setText(animalName);

            if (animalName.length()>4) {
                animalItemeImageView.setImageResource(resId);
            }
        }
    }
}
