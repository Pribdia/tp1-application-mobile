package com.example.tp1_application_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        // Construction liste animal
        AnimalList animalList = new AnimalList();

        // Contrcution et référence des views
        final TextView animalNameTextView = (TextView) findViewById(R.id.animalName);
        final TextView esperanceTextView = (TextView) findViewById(R.id.esperanceTextView);
        final TextView gestationTextView = (TextView) findViewById(R.id.gestationTextView);
        final TextView poidsNaissanceTextView = (TextView) findViewById(R.id.poidsNaissanceTextView);
        final TextView poidsAdulteTextView = (TextView) findViewById(R.id.poidsAdulteTextView);
        final TextView conservationPlainText = (TextView) findViewById(R.id.conservationText);

        final Button button = findViewById(R.id.sauvergardeBouton);

        ImageView animalImageView = (ImageView) findViewById(R.id.animalImageView);

        // Initialisation variable
        String animalName;
        final Animal animal ;

        // Verification et recuperation "Intent" animalName -> animalName
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                animalName = null;
            } else {
                animalName = extras.getString("animalName");
            }
        } else {
            animalName = (String) savedInstanceState.getSerializable("animalName");
        }

        // Modification du template de base en fonction de l'animal
        animal = animalList.getAnimal(animalName);
        if (animal != null) {

            // Changement des textView
            animalNameTextView.setText(animalName);
            esperanceTextView.setText(animal.getStrHightestLifespan());
            gestationTextView.setText(animal.getStrGestationPeriod());
            poidsNaissanceTextView.setText(animal.getStrBirthWeight());
            poidsAdulteTextView.setText(animal.getStrAdultWeight());
            conservationPlainText.setText(animal.getConservationStatus());

            // Changement image
            int resId = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
            animalImageView.setImageResource(resId);

        }

        // Fontion de sauvegarde
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                animal.setConservationStatus(conservationPlainText.getText().toString());
                Toast.makeText(AnimalActivity.this, "Sauvegarde effectué", Toast.LENGTH_LONG).show();
            }
        });



    }
}
